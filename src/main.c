#include <stdio.h>
#include <unistd.h>

#include "lvgl.h"
#include "wayland.h"

#define SCREEN_HOR_SIZE (800)
#define SCREEN_VER_SIZE (480)

static void event_handler(lv_event_t * e)
{
    lv_event_code_t code = lv_event_get_code(e);

    if(code == LV_EVENT_CLICKED) {
        LV_LOG_USER("Clicked");
    }
    else if(code == LV_EVENT_VALUE_CHANGED) {
        LV_LOG_USER("Toggled");
    }
}

static void lv_example_btn_1(void)
{
    lv_obj_t * label;

    lv_obj_t * btn1 = lv_btn_create(lv_scr_act());
    lv_obj_add_event_cb(btn1, event_handler, LV_EVENT_ALL, NULL);
    lv_obj_align(btn1, LV_ALIGN_CENTER, 0, -40);

    label = lv_label_create(btn1);
    lv_label_set_text(label, "Button");
    lv_obj_center(label);

    lv_obj_t * btn2 = lv_btn_create(lv_scr_act());
    lv_obj_add_event_cb(btn2, event_handler, LV_EVENT_ALL, NULL);
    lv_obj_align(btn2, LV_ALIGN_CENTER, 0, 40);
    lv_obj_add_flag(btn2, LV_OBJ_FLAG_CHECKABLE);
    lv_obj_set_height(btn2, LV_SIZE_CONTENT);

    label = lv_label_create(btn2);
    lv_label_set_text(label, "Toggle");
    lv_obj_center(label);
}

int main(int argc, char **argv)
{
	printf("LVGL Wayland demo\n");

	lv_init();
	lv_wayland_init();

	/* Create a display */
	lv_disp_t * disp = lv_wayland_create_window(SCREEN_HOR_SIZE, SCREEN_VER_SIZE, "lvgl wayland demo", NULL /*close_cb*/);
	//lv_wayland_window_set_fullscreen(true);

	///* Init the screen driver */
	//lv_disp_drv_init(&disp_drv);
	//disp_drv.flush_cb = _lvgl_write_on_screen;
	//disp_drv.draw_buf = &disp_buf;
	//disp_drv.hor_res = SCREEN_HOR_SIZE;
	//disp_drv.ver_res = SCREEN_VER_SIZE;
	//disp_drv.sw_rotate = false;
	//disp_drv.antialiasing = true;
	//lv_disp_drv_register(&disp_drv);

	///* Init the input driver (touchpad) */
	//lv_indev_drv_init(&indev_drv);
	//indev_drv.type = LV_INDEV_TYPE_POINTER;
	//indev_drv.read_cb = _touchpad_read;
	//lv_indev_drv_register(&indev_drv);

	lv_example_btn_1();

	while(1)
	{
		pause();
	}

	return 0;
}
