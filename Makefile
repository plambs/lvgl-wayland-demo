BIN = lvgl-wayland-demo
DESTDIR ?= /usr/bin

SRC = src/main.c \
      lvgl/lvgl_drivers/wayland/wayland.c \

INCLUDE = -Isrc \
          -Ilvgl/lvgl \
		  -Ilvgl/lvgl_drivers/wayland \

# Source file list to be builded
CFILES += $(SRC)

# Include LVGL source in the build
LVGL_DIR = lvgl/
LVGL_DIR_NAME = lvgl
LV_CONF_PATH = src/
include lvgl/lvgl/lvgl.mk 
CFILES += ${CSRCS}

CC ?= gcc
CCLD ?= gcc

CFLAGS += -DLV_CONF_INCLUDE_SIMPLE -DLV_LVGL_H_INCLUDE_SIMPLE $(INCLUDE)
LDFLAGS += -lwayland-client -lxkbcommon -lwayland-cursor

OBJS = $(patsubst %.c, %.o, $(CFILES))

all: $(BIN)

%.o : %.c
	echo "BUILD $(CC) -c $(CFLAGS) $< -o $@"
	$(CC) -c $(CFLAGS) $< -o $@

$(BIN): $(OBJS)
	$(CCLD) $(OBJS) -o $@ $(LDFLAGS)

install:
	install -D $(BIN) $(DESTDIR)/$(BIN)

clean:
	rm -f $(OBJS) $(BIN)

.PHONY: all clean
